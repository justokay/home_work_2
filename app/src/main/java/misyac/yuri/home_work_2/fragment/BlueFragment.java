package misyac.yuri.home_work_2.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import misyac.yuri.home_work_2.R;
import misyac.yuri.home_work_2.utils.ViewUtil;

public class BlueFragment extends Fragment {

    public static final String TAG = BlueFragment.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return ViewUtil.createSimpleView(getActivity().getApplicationContext(), R.color.material_blue);
    }

}
