package misyac.yuri.home_work_2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import misyac.yuri.home_work_2.fragment.BlueFragment;
import misyac.yuri.home_work_2.fragment.GreenFragment;
import misyac.yuri.home_work_2.fragment.RedFragment;

public class MainActivity extends AppCompatActivity {

    private CheckBox mRed;
    private CheckBox mGreen;
    private CheckBox mBlue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar(((Toolbar) findViewById(R.id.toolbar)));

        initView();

        setupView();
    }

    @Override
    public void onBackPressed() {
        checkTopStackEntry();
        super.onBackPressed();
    }

    private void initView() {
        mRed = (CheckBox) findViewById(R.id.red_check);
        mGreen = (CheckBox) findViewById(R.id.green_check);
        mBlue = (CheckBox) findViewById(R.id.blue_check);
    }

    private void setupView() {
        mRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    addFragment(RedFragment.TAG, new RedFragment());
                } else {
                    checkTopStackEntry(RedFragment.TAG);
                }
            }
        });

        mGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    addFragment(GreenFragment.TAG, new GreenFragment());
                } else {
                    checkTopStackEntry(GreenFragment.TAG);
                }
            }
        });

        mBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    addFragment(BlueFragment.TAG, new BlueFragment());
                } else {
                    checkTopStackEntry(BlueFragment.TAG);
                }
            }
        });

    }

    private void checkTopStackEntry(String tag) {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
            return;
        }

        FragmentManager.BackStackEntry stackEntry = getSupportFragmentManager()
                .getBackStackEntryAt(
                        getSupportFragmentManager().getBackStackEntryCount() - 1
                );

        if (stackEntry.getName().equals(tag)) {
            getSupportFragmentManager().popBackStack();
            addFragmentToFront();
        }
    }

    private void checkTopStackEntry() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
            return;
        }

        FragmentManager.BackStackEntry stackEntry = getSupportFragmentManager()
                .getBackStackEntryAt(
                        getSupportFragmentManager().getBackStackEntryCount() - 1
                );

        if (stackEntry.getName().equals(RedFragment.TAG) && mRed.isChecked()) {
            mRed.setChecked(false);
        } else if (stackEntry.getName().equals(GreenFragment.TAG) && mGreen.isChecked()) {
            mGreen.setChecked(false);
        } else if (stackEntry.getName().equals(BlueFragment.TAG) && mBlue.isChecked()) {
            mBlue.setChecked(false);
        }
        addFragmentToFront();

    }

    private void addFragmentToFront() {
        getSupportFragmentManager().executePendingTransactions();

        int entryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (entryCount <= 0) {
            return;
        }

        FragmentManager.BackStackEntry stackEntry = getSupportFragmentManager().getBackStackEntryAt(entryCount - 1);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(stackEntry.getName());
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .replace(R.id.color_container, fragment, stackEntry.getName())
                    .commit();
        }
    }

    private void addFragment(String tag, Fragment fragment) {
        Fragment foundFragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (foundFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(tag)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .replace(R.id.color_container, foundFragment, tag)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(tag)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .replace(R.id.color_container, fragment, tag)
                    .commit();
        }
    }
}
