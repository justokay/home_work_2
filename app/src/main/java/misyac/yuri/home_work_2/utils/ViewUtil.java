package misyac.yuri.home_work_2.utils;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;

public class ViewUtil {

    private ViewUtil() {
    }

    public static View createSimpleView(Context context, @ColorRes int id) {
        View view = new View(context);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        view.setLayoutParams(lp);
        view.setBackgroundColor(ContextCompat.getColor(context, id));
        view.setAlpha(0.5f);
        return view;
    }
}
